/*******************************************************************************
 * All right, title and interest in and to the software (the "Software") and the 
 * accompanying documentation or materials (the "Documentation"),
 * including all proprietary rights, therein including all patent rights,
 * trade secrets, trademarks and copyrights, shall remain the exclusive        
 * property of Pablo A. Carbajal Siller.
 * No interest, license or any right respecting the Software and the 
 * Documentation is granted by implication or otherwise.                
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from 
 * the Pablo A. Carbajal Siller.
 *                                                               
 * (C) Copyright 2017 Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 *******************************************************************************/
package com.xsolcorp.pathfinder.wim;

import java.util.Iterator;
import java.util.Map;

import com.solcorp.ingenium.gateway.IngeniumSessionStarter;
import com.solcorp.ingenium.gateway.SessionStarterContext;
import com.solcorp.ingenium.gateway.SessionStarterContext.Data;
import com.solcorp.pathfinder.access.AccessController;
import com.solcorp.pathfinder.access.AccessControllerException;
import com.solcorp.pathfinder.definitions.Process;
import com.solcorp.pathfinder.definitions.SessionDefs;
import com.solcorp.pathfinder.definitions.SessionVariable;
import com.solcorp.pathfinder.definitions.SessionVariableDef;
import com.solcorp.pathfinder.uidefs.UIStep;
import com.solcorp.pathfinder.util.logging.Logger;
import com.solcorp.util.Debug;
import com.solcorp.util.configuration.ConfigurationException;

/**
 * This {@link AccessController} creates INGENIUM sessions for incoming Headless
 * web services requests.
 * 
 * @author Pablo A. Carbajal Siller
 *
 */
public class FlowAccessController extends AbstractAccessController {

    private static final Logger LOG = Logger.getLogger(FlowAccessController.class);

    @Override
    public void preExecute(UIStep inputData) throws AccessControllerException {
	if (LOG.isDebugEnabled()) {
	    LOG.debug(Debug.entering());
	}

	SessionStarterContext sessionStarterContext = new SessionStarterContext();

	String sessionId = Process.createProcessID();
	inputData.setSessionId(sessionId);
	sessionStarterContext.setSessionId(sessionId);

	String uniqueName = getUniqueSecurityName(inputData);

	try {
	    LOG.info("XSOLCORP: Retrieving the Ingenium attributes from AD.");
	    Map<String, String> ldapAttributes = retrievePathFinderLdapAttributes(uniqueName);

	    LOG.info("XSOLCORP: Adding Ingenium attributes from AD into the UIStep object.");
	    addLdapAttributesToInputData(inputData, ldapAttributes);

	} catch (Exception e) {
	    String message = "Unable to retrieve the LDAP attributes.";
	    LOG.error("XSOLCORP: " + message + " Cause: " + e.getMessage());
	    throw new AccessControllerException(message, e);
	}

	String userName = inputData.getSessionVariableValue(getUserIdField());

	if (LOG.isDebugEnabled()) {
	    LOG.debug("Using " + userName + " as INGENIUM user name");
	}

	// String value = getLocaleId(sessionStarterContext);
	// inputData.setLocaleId(value);
	// if (LOG.isDebugEnabled()) {
	// LOG.debug("localId = " + value);
	// }

	IngeniumSessionStarter sessionStarter;
	try {
	    sessionStarter = new IngeniumSessionStarter(getConfig());

	} catch (ConfigurationException e) {
	    String message = "Unable to instantiate the IngeniumSessionStarter.";
	    LOG.error("XSOLCORP: " + message + " Cause: " + e.getMessage());
	    throw new AccessControllerException(message, e);
	}

	Iterator<SessionVariable> inputDataIterator = inputData.getSessionVariables().iterator();
	while (inputDataIterator.hasNext()) {
	    SessionVariable sessionVariable = (SessionVariable) inputDataIterator.next();
	    String name = sessionVariable.getName();
	    String value = sessionVariable.getStringValue();
	    if (LOG.isDebugEnabled()) {
		LOG.debug("Adding session variable to SessionStartedContext: " + name + ", " + value);
	    }

	    sessionStarterContext.addInputData(name, value);
	}

	sessionStarterContext.setUserName(userName);

	LOG.debug("Printing input data");
	for (Data dataTemp : sessionStarterContext.getInputData()) {
	    LOG.debug(dataTemp.getName() + ", " + dataTemp.getValue());
	}

	sessionStarterContext.addOutputData(getUserIdField(), userName);
	LOG.debug("Printing output data");
	for (Data dataTemp : sessionStarterContext.getOutputData()) {
	    LOG.debug(dataTemp.getName() + ", " + dataTemp.getValue());
	}

	if (!sessionStarter.startSession(sessionStarterContext)) {
	    String message = "Unable to start the session with the IngeniumSessionStarter.";
	    LOG.error("XSOLCORP: " + message);
	    throw new AccessControllerException(message);
	}

	Iterator<Data> outputDataIterator = sessionStarterContext.getOutputData().iterator();
	while (outputDataIterator.hasNext()) {
	    SessionStarterContext.Data outputData = outputDataIterator.next();
	    SessionVariable sessionVariable = createSessionVariable(outputData);
	    if (sessionVariable.isShared()) {
		inputData.setSessionVariable(sessionVariable);
	    }
	    if (LOG.isDebugEnabled()) {
		LOG.debug("SessionVariable name:" + outputData.getName());
		LOG.debug("SessionVariable value:" + outputData.getValue());
	    }
	}
    }

    private SessionVariable createSessionVariable(SessionStarterContext.Data sessionVariableData) {
	String variableName = sessionVariableData.getName();
	String variableValue = sessionVariableData.getValue();

	SessionVariableDef sessionDef = SessionDefs.getInstance().get(variableName);
	if (sessionDef != null) {
	    return sessionDef.createSessionVariable(variableValue);
	}
	return new SessionVariable(variableName, variableValue);
    }

}
