/*******************************************************************************
 * All right, title and interest in and to the software (the "Software") and the 
 * accompanying documentation or materials (the "Documentation"),
 * including all proprietary rights, therein including all patent rights,
 * trade secrets, trademarks and copyrights, shall remain the exclusive        
 * property of Pablo A. Carbajal Siller.
 * No interest, license or any right respecting the Software and the 
 * Documentation is granted by implication or otherwise.                
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from 
 * the Pablo A. Carbajal Siller.
 *                                                               
 * (C) Copyright 2017 Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 *******************************************************************************/
package com.xsolcorp.pathfinder.wim;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.security.auth.Subject;
import javax.security.auth.login.CredentialExpiredException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import com.ibm.websphere.security.WSSecurityException;
import com.ibm.websphere.security.auth.CredentialDestroyedException;
import com.ibm.websphere.security.auth.WSSubject;
import com.ibm.websphere.security.cred.WSCredential;
import com.solcorp.ingenium.gateway.IngeniumSessionStarter;
import com.solcorp.ingenium.gateway.SessionStarterContext;
import com.solcorp.ingenium.gateway.SessionStarterContext.Data;
import com.solcorp.pagebuilder.PageBuilderConfig;
import com.solcorp.pagebuilder.PageBuilderSession;
import com.solcorp.pagebuilder.RequestContext;
import com.solcorp.pagebuilder.Requester;
import com.solcorp.pagebuilder.authenticators.Authenticator;
import com.solcorp.pathfinder.definitions.SessionDefs;
import com.solcorp.pathfinder.definitions.SessionVariable;
import com.solcorp.pathfinder.definitions.SessionVariableDef;
import com.solcorp.pathfinder.uidefs.UIStep;
import com.solcorp.pathfinder.util.logging.Logger;
import com.solcorp.util.Debug;
import com.solcorp.util.StringUtil;
import com.solcorp.util.configuration.Configuration;
import com.solcorp.util.configuration.ConfigurationException;

/**
 * This {@code Authenticator} retrieves Ingenium attributes from an Active
 * Directory via WebSphere's Identity Manager (WIM) and then creates an Ingenium
 * session. The Ingenium attributes are specified in the PathFinderConfig.xml
 * file.
 * 
 * @author Pablo Carbajal
 *
 */
public class PageBuilderAuthenticator implements Authenticator {

    private static final Logger LOG = Logger.getLogger(PageBuilderAuthenticator.class);

    private static final String FILTER_STRING = "filterString";
    private static final String PARAMETER_LOCALEID = "LocaleID";
    private static final String SEARCH_CONTEXT = "SearchContext";
    private static final String USER_ID_FIELD = "UserIdField";
    private String m_filterString;
    private final Properties m_initialProperties = new Properties();
    private String m_searchContext;
    private IngeniumSessionStarter m_sessionStarter;
    private String m_userIdField;

    private WebSphereIdentityManager m_wimClient;

    @Override
    public boolean authenticate(RequestContext context) {
	if (LOG.isDebugEnabled()) {
	    LOG.debug(Debug.entering());
	}

	if (isIngeniumSessionCreated(context)) {
	    return true;
	}

	UIStep inputData = context.getInputData();

	SessionStarterContext sessionStarterContext = new SessionStarterContext();

	sessionStarterContext.setSessionId(inputData.getSessionId());
	try {
	    Map<String, String> pathFinderLdapMap = m_wimClient
		    .retrievePathFinderLdapAttributes(getUniqueSecurityName());

	    LOG.debug("Adding LDAP attributes to the INGENIUM context.");
	    for (Map.Entry<String, String> attributes : pathFinderLdapMap.entrySet()) {
		String pathFinderName = attributes.getKey();
		String ldapAttrValue = attributes.getValue();

		sessionStarterContext.addInputData(pathFinderName, ldapAttrValue);

		if (pathFinderName.equals(this.m_userIdField)) {
		    sessionStarterContext.setUserName(ldapAttrValue);

		    if (LOG.isDebugEnabled()) {
			LOG.debug("Using " + ldapAttrValue + " as INGENIUM user name");
		    }
		}
	    }

	    String value = getLocaleId(sessionStarterContext);
	    inputData.setLocaleId(value);
	    if (LOG.isDebugEnabled()) {
		LOG.debug("localId = " + value);
	    }

	    if (!this.m_sessionStarter.startSession(sessionStarterContext)) {
		invalidateSession(context);
		return false;
	    }
	    Requester requester = context.getRequester();

	    inputData.setSessionVariable(this.m_userIdField, sessionStarterContext.getUserName());
	    requester.setSessionVariable(this.m_userIdField, sessionStarterContext.getUserName());

	    Iterator<Data> localIterator = sessionStarterContext.getOutputData().iterator();
	    while (localIterator.hasNext()) {
		SessionStarterContext.Data outputData = localIterator.next();
		SessionVariable sessionVariable = createSessionVariable(outputData);
		if (sessionVariable.isShared()) {
		    inputData.setSessionVariable(sessionVariable);
		}
		requester.setSessionVariable(outputData.getName(), outputData.getValue());
		if (LOG.isDebugEnabled()) {
		    LOG.debug("SessionVariable name:" + outputData.getName());
		    LOG.debug("SessionVariable value:" + outputData.getValue());
		}
	    }

	    PageBuilderSession pageBuilderSession = context.getPageBuilderSession();
	    pageBuilderSession.updateSharedSessionVariables(inputData);

	    setIngeniumSessionCreated(context);

	} catch (Exception e) {
	    LOG.error("XSOLCORP: Unable to retrieve the LDAP attributes. Cause: " + e.getMessage());
	    sendErrorPage(context);
	    invalidateSession(context);
	    return false;
	}

	return true;
    }

    private SessionVariable createSessionVariable(SessionStarterContext.Data sessionVariableData) {
	String variableName = sessionVariableData.getName();
	String variableValue = sessionVariableData.getValue();

	SessionVariableDef sessionDef = SessionDefs.getInstance().get(variableName);
	if (sessionDef != null) {
	    return sessionDef.createSessionVariable(variableValue);
	}
	return new SessionVariable(variableName, variableValue);
    }

    public String getLocaleId(SessionStarterContext context) {
	context.getInputData();
	for (Data contextData : context.getInputData()) {
	    if (contextData.getName().equals(PARAMETER_LOCALEID)) {
		LOG.debug("Using localeId from the context.");
		return contextData.getValue();
	    }
	}

	LOG.debug("Using default localeId.");
	return PageBuilderConfig.getInstance().getString("Common.DefaultLocaleId");
    }

    @Override
    public boolean init(ServletContext context, Configuration config) {
	if (LOG.isDebugEnabled()) {
	    LOG.debug(Debug.entering());
	}
	if (!setConfigurationParameters(config)) {
	    LOG.debug("setConfigurationParameters() return false");
	    return false;
	}
	if (!setInitialProperties(config)) {
	    LOG.debug("setInitialProperties() return false");
	    return false;
	}
	try {
	    m_wimClient = new WebSphereIdentityManager(config);

	} catch (ConfigurationException exc) {
	    LOG.error(exc.getMessage());
	    return false;
	}
	try {
	    this.m_sessionStarter = new IngeniumSessionStarter(config);
	} catch (ConfigurationException exc) {
	    LOG.error(exc.getMessage());
	    return false;
	}
	return true;
    }

    private void invalidateSession(RequestContext context) {
	context.getRequest().getSession().invalidate();
    }

    @Override
    public boolean isAuthenticated(RequestContext requestContext) {
	return true;
    }

    private boolean isIngeniumSessionCreated(RequestContext context) {
	HttpSession session = context.getRequest().getSession();
	return session.getAttribute("INGENIUM_SESSION_CREATED") != null;
    }

    private void logError(String error) {
	LOG.error("XSOLCORP: " + error);
    }

    private void sendErrorPage(RequestContext context) {
	if (LOG.isDebugEnabled()) {
	    LOG.debug(Debug.entering());
	}
	String responsePage = context.getResponsePage(context.getInputData());
	context.setLastResponsePage(responsePage);

	context.getResponse().addHeader("Cache-Control", "no-store");
	context.getResponse().setDateHeader("Last-Modified", System.currentTimeMillis());
	context.getResponse().setDateHeader("Expires", -1L);

	RequestDispatcher dispatcher = context.getRequest().getRequestDispatcher(responsePage);
	try {
	    if (dispatcher != null) {
		dispatcher.forward(context.getRequest(), context.getResponse());
	    } else {
		context.getResponse().sendError(404);
		LOG.error("XSOLCORP: Unable to create a RequestDispatcher for resource " + responsePage);
	    }
	} catch (ServletException exc) {
	    String message = "RequestDispatcher.forward() failed: " + exc.getMessage();
	    LOG.error("XSOLCORP: " + message);
	} catch (IOException exc) {
	    String message = "RequestDispatcher.forward() failed: " + exc.getMessage();
	    LOG.error("XSOLCORP: " + message);
	}
	if (LOG.isDebugEnabled()) {
	    LOG.debug(Debug.exiting());
	}
    }

    private boolean setConfigurationParameters(Configuration config) {
	if (LOG.isDebugEnabled()) {
	    LOG.debug(Debug.entering());
	}
	this.m_searchContext = config.getString("Ingenium.Ldap.SearchContext");

	this.m_filterString = config.getString("Ingenium.Ldap.FilterString");

	this.m_userIdField = config.getString("Common.UserIdFieldName");
	if (LOG.isDebugEnabled()) {
	    LOG.debug("m_searchContext:" + String.valueOf(this.m_searchContext));
	    LOG.debug("m_filterString:" + String.valueOf(this.m_filterString));
	    LOG.debug("m_userIdField:" + String.valueOf(this.m_userIdField));
	}
	if ((this.m_searchContext == null) || (this.m_filterString == null) || (this.m_userIdField == null)) {
	    StringBuffer message = new StringBuffer();

	    message.append("One of the required configuration elements in the System section is missing.");
	    message.append(StringUtil.LINE_SEPARATOR);
	    message.append("Configuration parameters:").append(StringUtil.LINE_SEPARATOR);

	    message.append(SEARCH_CONTEXT).append(": ");
	    if (this.m_searchContext != null) {
		message.append(this.m_searchContext);
	    }
	    message.append(StringUtil.LINE_SEPARATOR);

	    message.append(FILTER_STRING).append(": ");
	    if (this.m_filterString != null) {
		message.append(this.m_filterString);
	    }
	    message.append(StringUtil.LINE_SEPARATOR);

	    message.append(USER_ID_FIELD).append(": ");
	    if (this.m_userIdField != null) {
		message.append(this.m_userIdField);
	    }
	    message.append(StringUtil.LINE_SEPARATOR);

	    logError(message.toString());
	    return false;
	}
	return true;
    }

    private void setIngeniumSessionCreated(RequestContext context) {
	HttpSession session = context.getRequest().getSession();
	session.setAttribute("INGENIUM_SESSION_CREATED", Boolean.TRUE);
    }

    private boolean setInitialProperties(Configuration config) {
	if (LOG.isDebugEnabled()) {
	    LOG.debug(Debug.entering());
	}

	String[] keys = config.getStringArray("Ingenium.Ldap.InitialContext.Property.Name");
	String[] values = config.getStringArray("Ingenium.Ldap.InitialContext.Property.Value");
	if (LOG.isDebugEnabled()) {
	    LOG.debug("Number of keys:" + keys.length);
	    LOG.debug("Number of values:" + values.length);
	}
	if (keys.length != values.length) {
	    logError("XSOLCORP: Configuration error in the Ingenium.Ldap.InitialContext.Property properties.");
	    return false;
	}
	for (int index = 0; index < keys.length; index++) {
	    if (LOG.isDebugEnabled()) {
		LOG.debug("Setting property " + index);
		LOG.debug("Key " + String.valueOf(keys[index]));
		LOG.debug("Value " + String.valueOf(values[index]));
	    }
	    this.m_initialProperties.setProperty(keys[index], values[index]);
	}
	return true;
    }

    @Override
    public String status() {
	StringBuffer buffer = new StringBuffer();

	buffer.append("\tUser Authenticator").append(StringUtil.LINE_SEPARATOR);
	buffer.append("\t\tAuthenticator Class:  ").append(getClass().getName()).append(StringUtil.LINE_SEPARATOR);
	buffer.append("Directory in use: ").append(this.m_initialProperties.get("java.naming.provider.url"));

	return buffer.toString();
    }

    private String getUniqueSecurityName()
	    throws WSSecurityException, CredentialDestroyedException, CredentialExpiredException {
	Subject callerSubject = WSSubject.getCallerSubject();

	if (callerSubject == null) {
	    LOG.error(
		    "XSOLCORP: Unable to get the caller subject, hence its credentials. Are you unauthenticated while calling this method?");
	    return null;
	}

	WSCredential callerCred = callerSubject.getPublicCredentials(WSCredential.class).iterator().next();
	return callerCred.getUniqueSecurityName();
    }

}
