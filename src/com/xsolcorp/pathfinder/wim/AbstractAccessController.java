/*******************************************************************************
 * All right, title and interest in and to the software (the "Software") and the 
 * accompanying documentation or materials (the "Documentation"),
 * including all proprietary rights, therein including all patent rights,
 * trade secrets, trademarks and copyrights, shall remain the exclusive        
 * property of Pablo A. Carbajal Siller.
 * No interest, license or any right respecting the Software and the 
 * Documentation is granted by implication or otherwise.                
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from 
 * the Pablo A. Carbajal Siller.
 *                                                               
 * (C) Copyright 2017 Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 *******************************************************************************/
package com.xsolcorp.pathfinder.wim;

import java.security.PrivilegedActionException;
import java.util.Map;

import javax.security.auth.login.CredentialExpiredException;

import com.ibm.websphere.security.WSSecurityException;
import com.ibm.websphere.security.auth.CredentialDestroyedException;
import com.ibm.websphere.wim.exception.WIMException;
import com.solcorp.pathfinder.access.AccessController;
import com.solcorp.pathfinder.access.AccessControllerException;
import com.solcorp.pathfinder.uidefs.UIStep;
import com.solcorp.pathfinder.util.logging.Logger;
import com.solcorp.util.Debug;
import com.solcorp.util.StringUtil;
import com.solcorp.util.configuration.Configuration;
import com.solcorp.util.configuration.ConfigurationException;

/**
 * This abstract {@link AccessController} provides initialization and retrieval
 * of LDAP attributes for subclasses.
 * 
 * @author Pablo A. Carbajal Siller
 *
 */
public abstract class AbstractAccessController implements AccessController {

    private static final Logger LOG = Logger.getLogger(AbstractAccessController.class);

    private String m_dnTemplate;
    private String m_searchContext;
    private String m_userIdField;
    private WebSphereIdentityManager m_wimClient;

    private Configuration m_config;

    @Override
    public boolean init(Configuration config) {
	m_config = config;

	try {
	    m_wimClient = new WebSphereIdentityManager(config);

	} catch (ConfigurationException exc) {
	    LOG.error(exc.getMessage());
	    return false;
	}

	m_dnTemplate = getConfigElementValue(m_config, "Ingenium.Ldap.DistinguishedNameTemplate");
	m_searchContext = getConfigElementValue(this.m_config, "Ingenium.Ldap.SearchContext");
	m_userIdField = getConfigElementValue(this.m_config, "Common.UserIdFieldName");

	if (m_searchContext == null || m_dnTemplate == null) {
	    return false;
	}

	return true;
    }

    @Override
    public void postExecute(UIStep inputData, UIStep outputData) throws AccessControllerException {
	// Do nothing
    }

    @Override
    public abstract void preExecute(UIStep inputData) throws AccessControllerException;

    /**
     * Calling this method will add the provided map of LDAP attributes to the
     * specified input UIStep object.
     * 
     * @param inputData
     *            The {@link UIStep} object to add the map of LDAP attributes
     * @param ldapAttributes
     *            The map of LDAP attributes to be added to the UIStep input
     *            object.
     */
    protected void addLdapAttributesToInputData(UIStep inputData, Map<String, String> ldapAttributes) {
	LOG.debug(Debug.entering());
	for (Map.Entry<String, String> attributes : ldapAttributes.entrySet()) {
	    String pathFinderName = attributes.getKey();
	    String stringValue = attributes.getValue();
	    if (LOG.isDebugEnabled()) {
		LOG.debug("SessionVariable name: " + pathFinderName);
		LOG.debug("SessionVariable value: " + stringValue);
	    }
	    inputData.setSessionVariable(pathFinderName, stringValue);
	}
	LOG.debug(Debug.exiting());
    }

    /**
     * Retrieves the specified element name from the given {@link Configuration}
     * object.
     * 
     * @param config
     *            The {@link Configuration} object with available PathFinder
     *            configuration values.
     * @param elementName
     *            The name of the configuration value to retrieve.
     * @return The value of the configuration element.
     */
    private String getConfigElementValue(Configuration config, String elementName) {
	String elementValue = config.getString(elementName);
	if (elementValue == null) {
	    LOG.error("The configuration element " + elementName
		    + " has not been specified in the PathFinder configuration file");
	}

	return elementValue;
    }

    /**
     * Returns the {@link Configuration} object with available PathFinder
     * configuration values.
     */
    protected Configuration getConfig() {
	return m_config;
    }

    /**
     * Returns the field that identifies the user id.
     */
    protected String getUserIdField() {
	return m_userIdField;
    }

    /**
     * Returns the configuration value for the distinguished name template.
     */
    protected String getDnTemplate() {
	return m_dnTemplate;
    }

    /**
     * Returns the configuration value for the search context.
     */
    protected String getSearchContext() {
	return m_searchContext;
    }

    /**
     * Retrieves via WebSphere Identity Manager the map of LDAP attributes as
     * specified in the PathFinderConfig file.
     * 
     * @param uniqueName
     *            The unique name that WebSphere Identity Manager will use for
     *            the data object graph.
     * @return A map containing the LDAP attributes.
     * @throws CredentialExpiredException
     *             if this method can't retrieve the LDAP attributes.
     * @throws CredentialDestroyedException
     *             if this method can't retrieve the LDAP attributes.
     * @throws WSSecurityException
     *             if this method can't retrieve the LDAP attributes.
     * @throws WIMException
     *             if this method can't retrieve the LDAP attributes.
     * @throws PrivilegedActionException
     *             if this method can't retrieve the LDAP attributes.
     */
    protected Map<String, String> retrievePathFinderLdapAttributes(String uniqueName) throws CredentialExpiredException,
	    CredentialDestroyedException, WSSecurityException, WIMException, PrivilegedActionException {
	return m_wimClient.retrievePathFinderLdapAttributes(uniqueName);
    }

    /**
     * This method retrieves the unique security name from the input UIStep
     * object. This unique name is a well formed distinguished name based on the
     * user id field, the distinguished name template and search context all
     * found in the PathFinderConfig file.
     * 
     * @param inputData
     *            The input {@link UIStep} object containing the user id.
     * @return The unique security name to be used when calling the LDAP
     *         directory.
     * @throws AccessControllerException
     *             if the user ID was not found in the input data.
     */
    protected String getUniqueSecurityName(UIStep inputData) throws AccessControllerException {
	String userName = inputData.getVariableValue(getUserIdField());
	if (userName == null) {
	    userName = inputData.getSessionVariableValue(getUserIdField());
	}
	if (userName == null) {
	    String message = "The user ID variable " + getUserIdField() + " was not found in the incoming message.";
	    LOG.error("XSOLCORP: " + message);
	    throw new AccessControllerException(message);
	}

	String uniqueName = StringUtil.replaceAll(getDnTemplate(), "%UserIdField%", userName);
	uniqueName = StringUtil.replaceAll(uniqueName, "%SearchContext%", getSearchContext());
	return uniqueName;
    }
}
