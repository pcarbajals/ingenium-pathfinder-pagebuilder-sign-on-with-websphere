/*******************************************************************************
 * All right, title and interest in and to the software (the "Software") and the 
 * accompanying documentation or materials (the "Documentation"),
 * including all proprietary rights, therein including all patent rights,
 * trade secrets, trademarks and copyrights, shall remain the exclusive        
 * property of Pablo A. Carbajal Siller.
 * No interest, license or any right respecting the Software and the 
 * Documentation is granted by implication or otherwise.                
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from 
 * the Pablo A. Carbajal Siller.
 *                                                               
 * (C) Copyright 2017 Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 *******************************************************************************/
package com.xsolcorp.pathfinder.wim;

import java.util.Map;

import com.solcorp.pathfinder.access.AccessController;
import com.solcorp.pathfinder.access.AccessControllerException;
import com.solcorp.pathfinder.uidefs.UIStep;
import com.solcorp.pathfinder.util.logging.Logger;
import com.solcorp.util.Debug;

/**
 * This {@link AccessController} creates INGENIUM sessions for incoming Single
 * P-Step web services requests.
 * 
 * @author Pablo A. Carbajal Siller
 *
 */
public class PStepAccessController extends AbstractAccessController {
    static final Logger LOG = Logger.getLogger(PStepAccessController.class);

    @Override
    public void preExecute(UIStep inputData) throws AccessControllerException {
	try {
	    String uniqueName = getUniqueSecurityName(inputData);
	    LOG.info("XSOLCORP: Retrieving the Ingenium attributes from AD.");
	    Map<String, String> ldapAttributes = retrievePathFinderLdapAttributes(uniqueName);

	    LOG.info("XSOLCORP: Adding Ingenium attributes from AD into the UIStep object.");
	    addLdapAttributesToInputData(inputData, ldapAttributes);

	} catch (Exception e) {
	    String errorMessage = "Unable to retrieve the LDAP attributes";
	    LOG.error("XSOLCORP: " + errorMessage + ". Cause: " + e.getMessage());
	    throw new AccessControllerException(errorMessage, e.getCause());
	}
	LOG.debug(Debug.exiting());
    }
}
