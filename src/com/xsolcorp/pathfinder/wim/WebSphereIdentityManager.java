/*******************************************************************************
 * All right, title and interest in and to the software (the "Software") and the 
 * accompanying documentation or materials (the "Documentation"),
 * including all proprietary rights, therein including all patent rights,
 * trade secrets, trademarks and copyrights, shall remain the exclusive        
 * property of Pablo A. Carbajal Siller.
 * No interest, license or any right respecting the Software and the 
 * Documentation is granted by implication or otherwise.                
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from 
 * the Pablo A. Carbajal Siller.
 *                                                               
 * (C) Copyright 2017 Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 *******************************************************************************/
/**
 * 
 */
package com.xsolcorp.pathfinder.wim;

import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.security.auth.login.CredentialExpiredException;

import com.ibm.websphere.security.WSSecurityException;
import com.ibm.websphere.security.auth.CredentialDestroyedException;
import com.ibm.websphere.wim.SchemaConstants;
import com.ibm.websphere.wim.client.LocalServiceProvider;
import com.ibm.websphere.wim.exception.WIMException;
import com.ibm.websphere.wim.util.SDOHelper;
import com.ibm.ws.security.core.ContextManagerFactory;
import com.solcorp.pathfinder.ldap.LdapDirectoryFieldMap;
import com.solcorp.pathfinder.ldap.LdapDirectoryFieldMaps;
import com.solcorp.pathfinder.util.logging.Logger;
import com.solcorp.util.configuration.Configuration;
import com.solcorp.util.configuration.ConfigurationException;

import commonj.sdo.DataObject;

/**
 * Instances of this class provide a interface to WebSphere Identity
 * Manager (WIM). Clients can use this class for retrieving the LDAP attributes specified in the
 * PathFinderConfig file.
 * 
 * @author Pablo A. Carbajal Siller
 *
 */
public class WebSphereIdentityManager {
    private static final Logger LOG = Logger.getLogger(WebSphereIdentityManager.class);

    private final LdapDirectoryFieldMaps m_directoryOutputMappings = new LdapDirectoryFieldMaps();

    public WebSphereIdentityManager(final Configuration config) throws ConfigurationException {
	m_directoryOutputMappings.init(config, "Ingenium.Ldap.DirectoryOutputFields");
    }

    /**
     * Retrieves via WebSphere Identity Manager the map of LDAP attributes as
     * specified in the PathFinderConfig file.
     * 
     * @param uniqueName
     *            The unique name that WebSphere Identity Manager will use for
     *            the data object graph.
     * @return A map containing the LDAP attributes.
     * @throws WIMException
     *             if this method can't call the Virtual Member Manager service.
     * @throws PrivilegedActionException
     *             if this method can't call the Virtual Member Manager service.
     * @throws CredentialExpiredException
     *             if this method can't call the Virtual Member Manager service.
     * @throws CredentialDestroyedException
     *             if this method can't call the Virtual Member Manager service.
     * @throws WSSecurityException
     *             if this method can't call the Virtual Member Manager service.
     */
    Map<String, String> retrievePathFinderLdapAttributes(final String uniqueName) throws WIMException,
	    PrivilegedActionException, CredentialExpiredException, CredentialDestroyedException, WSSecurityException {

	Map<String, String> pathFinderLdapAttributes = new HashMap<String, String>();

	// Get the unique name from the authenticated user's credentials
	if (uniqueName == null || uniqueName.isEmpty()) {
	    LOG.error("XSOLCORP: Unable to find the principal's unique name.");
	    return pathFinderLdapAttributes;

	} else if (LOG.isDebugEnabled()) {
	    LOG.debug("XSOLCORP: Getting LDAP attributes for uniqueName -> '" + uniqueName + "'");
	}

	// Build up the input data graph for getting the user's LDAP
	// attributes
	final DataObject root = SDOHelper.createRootDataObject();
	DataObject entity = SDOHelper.createEntityDataObject(root, null, SchemaConstants.DO_PERSON_ACCOUNT);
	entity.createDataObject(SchemaConstants.DO_IDENTIFIER).set(SchemaConstants.PROP_UNIQUE_NAME, uniqueName);
	DataObject propCtrl = SDOHelper.createControlDataObject(root, null, SchemaConstants.DO_PROPERTY_CONTROL);

	// Add LDAP attributes defined in PathFinderConfig into the input
	// data graph
	for (LdapDirectoryFieldMap mapping : m_directoryOutputMappings) {
	    String ldapName = mapping.getLdapName();
	    propCtrl.getList(SchemaConstants.PROP_PROPERTIES).add(ldapName);
	    if (LOG.isDebugEnabled()) {
		LOG.debug("Added attribute: " + String.valueOf(ldapName));
	    }
	}

	// Call the virtual member manager service with the identity of the
	// server subject (additional privileges)
	// to avoid error CWWIM2008E
	PrivilegedExceptionAction<DataObject> action = new PrivilegedExceptionAction<DataObject>() {
	    public DataObject run() throws Exception {
		LOG.debug("Calling the virtual member manager service.");
		return new LocalServiceProvider().get(root);
	    }
	};

	DataObject response = (DataObject) ContextManagerFactory.getInstance().runAsSystem(action);

	LOG.debug("Processing the response from the virtual member manager service.");
	// Get all entities in the DataObject
	List<DataObject> entities = response.getList(SchemaConstants.DO_ENTITIES);
	for (int i = 0; i < entities.size(); i++) {
	    DataObject ent = entities.get(i);

	    // Get the LDAP attributes
	    if (ent != null) {
		LOG.debug("Adding LDAP attributes to the INGENIUM context.");
		for (LdapDirectoryFieldMap mapping : m_directoryOutputMappings) {
		    String ldapName = mapping.getLdapName();
		    String pathFinderName = mapping.getPathFinderName();

		    String ldapAttrValue = ent.getString(ldapName);

		    if (LOG.isDebugEnabled()) {
			LOG.debug("Received " + ldapName + " as " + ldapAttrValue);
		    }

		    if (ldapAttrValue != null) {
			ldapAttrValue = ldapAttrValue.toUpperCase();

			if (LOG.isDebugEnabled()) {
			    LOG.debug("Converted to upper case " + ldapAttrValue);
			}
		    }

		    pathFinderLdapAttributes.put(pathFinderName, ldapAttrValue);

		    if (LOG.isDebugEnabled()) {
			LOG.debug("Set " + pathFinderName + " as " + ldapAttrValue);
		    }
		}
	    } else {
		LOG.error("XSOLCORP: Unable to process the response because the DO's entities element is missing.");
	    }
	}

	return pathFinderLdapAttributes;
    }
}
